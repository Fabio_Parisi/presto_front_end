let navbar=document.querySelector('.navbar');
let rowCard=document.querySelector('#row-card')

document.addEventListener('scroll', () =>{
    let scrolled=window.scrollY;
    if(scrolled>80){
        navbar.classList.add('sticky-top');
    }else navbar.classList.remove('sticky-top');
})

const swiper = new Swiper('.swiper', {
    // Optional parameters
    loop: true,
  
    // Navigation arrows
    navigation: {
      nextEl: '.button-next',
      prevEl: '.button-prev',
    },
  });

  let categories = [
    {name : 'Auto' , icon : `<i class="fa-solid fa-car-rear fs-3 ic"></i>` , announcementsCount : 123},
    {name : 'Elettronica' , icon : `<i class="fa-solid fa-laptop fs-3 ic"></i>` , announcementsCount : 564},
    {name : 'Moto' , icon : `<i class="fa-solid fa-motorcycle fs-3 ic"></i>` , announcementsCount : 230},
    {name : 'Abbigliamento' , icon : `<i class="fa-solid fa-shirt fs-3 ic"></i>` , announcementsCount : 321},
    {name : 'Sport' , icon : `<i class="fa-solid fa-person-running fs-3 ic"></i>` , announcementsCount : 90},
    {name : 'Giardinaggio' , icon : `<i class="fa-solid fa-leaf fs-3 ic"></i>` , announcementsCount : 50},
    {name : 'Casa' , icon : `<i class="fa-solid fa-house-chimney fs-3 ic"></i>` , announcementsCount : 134},
    {name : 'Cucina' , icon : `<i class="fa-solid fa-fire-burner fs-3 ic"></i>` , announcementsCount : 176},
]

categories.forEach((category)=>{
  let card=document.createElement('div');
  card.classList.add('col-12','col-md-6', 'col-lg-3','mt-3');
  card.innerHTML=`
  <div class="category-card">
  <div class="card-body">
    <div class="icon text-center">
      ${category.icon}
    </div>
    <div class="card-text text-center">
      <h5 class="mt-3">${category.name}</h5>
      <p class="mb-0">${category.announcementsCount} Articoli</p>
    </div>
  </div>
</div>
  `;
  rowCard.appendChild(card);
})

