fetch("../annunci.json")
.then((response) => response.json())
.then((annuncio) => {
  let cardRow = document.querySelector("#cardRow");
  let search = document.querySelector("#search");
  let btnSearch = document.querySelector("#btnSearch");
  let categoryForm = document.querySelector("#categoryForm");
  
  function createCards(an = annuncio) {
    cardRow.innerHTML = "";
    an.forEach((an) => {
      let card = document.createElement("div");
      card.classList.add('col-12', 'col-md-4','d-flex','justify-content-center');
      card.innerHTML = `
      <div class="cardmaster shadow">
      <div class="card-img position-relative">
        <img class="img-fluid" src="https://picsum.photos/400/224" alt="">
        <p class="${an.type == 'sell' ? 'sell bg-danger' : 'find bg-success'  }">${an.type== 'sell' ? 'Vendo' : 'Cerco'}</p>
      </div>
      <div class="card-text">
        <p class="price fs-4">${an.price} €</p> 
        <h5>${an.name}</h5>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
      </div>
      <div class="card-footer d-flex text-center">
        <div class="col category"><p class="my-3">${an.category}</p></div>
        <div class="col date"><p class="my-3">${randomDate(new Date(2022, 0, 1), new Date())}</p></div>
      </div>
    </div>
      `;
      cardRow.appendChild(card);
    });
  }

  function randomDate(start, end) {
    let date = new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
    return date.toLocaleDateString();
}



  
  function filterBySerach(annuncio) {
    let filteredSearch = [];
    annuncio.forEach((an) => {
      if (an.name.toLowerCase().includes(search.value.toLowerCase())) {
        filteredSearch.push(an);
      }
    });
    search.value = "";
    return filteredSearch;
  }
  
  function fillCategory() {
    let filteredCategory = new Set();
    annuncio.forEach((an) => {
      filteredCategory.add(an.category);
    });
    
    filteredCategory.forEach((cat) => {
      let option = document.createElement("option");
      option.innerHTML = cat;
      categoryForm.appendChild(option);
    });
  }
  
  function filterByCategory(annuncio) {
    const filteredCategory = annuncio.filter((an) => {
      return an.category == categoryForm.value || categoryForm.value === "tutti";
      });
      return filteredCategory;
    }
    
    function filterByPrice(annuncio){
      let minP=document.querySelector('#min');
      let maxP=document.querySelector('#max');
      let min=Number(minP.value);  
      let max= maxP.value == '' ? Infinity : Number(maxP.value);
      const filterdPrice= annuncio.filter(an =>{
        return Number(an.price)>=min && Number(an.price)<=max
      })
      return filterdPrice;
    }
    
    function sortAnnunci(annuncio){
      let sortFort=document.querySelector('#sortForm');
      
      switch(sortFort.value){
        case 'priceAsc':
        annuncio=annuncio.sort((a,b) => a.price - b.price);
        return annuncio;
        case 'priceDesc':
        annuncio=annuncio.sort((a,b) => b.price - a.price);
        return annuncio;
        case 'nameAsc':
        annuncio=annuncio.sort((a,b)=>{
          const nameA = a.name.toUpperCase(); // ignore upper and lowercase
          const nameB = b.name.toUpperCase(); // ignore upper and lowercase
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          
          // names must be equal
          return 0;
          
        })
        return annuncio;
        case 'nameDesc':
          annuncio=annuncio.sort((a,b)=>{
            const nameA = a.name.toUpperCase(); // ignore upper and lowercase
            const nameB = b.name.toUpperCase(); // ignore upper and lowercase
            if (nameA < nameB) {
              return 1;
            }
            if (nameA > nameB) {
              return -1;
            }
            
            // names must be equal
            return 0;
            
          })
          return annuncio;
      }
    }
    
    
    function findAnnuncio() {
      let findBySearch = filterBySerach(annuncio);
      let findByCategory = filterByCategory(findBySearch);
      let findByPrice= filterByPrice(findByCategory);
      let sortAnnouncments=sortAnnunci(findByPrice);
      createCards(sortAnnouncments);
    }
    
    btnSearch.addEventListener("click", findAnnuncio);
    
    createCards();
    fillCategory();
    
  });